import React, { Component } from 'react';

export default class Profile extends Component {
  render() {
    const style_logo = {
      container: {
        display: 'inline-block',
        width: '10%'
      }
    }

    return (
      <div style={style_logo.container}>
        <a href="#">Profile</a>
      </div>
    );
  }
}
