import React, { Component } from 'react';

export default class Logo extends Component {
  render() {
    const style_search ={
      box: {
      display: 'inline-block',
      height: '10px',
      width: '40%',
      marginLeft: '10%',

    },

     color: {
      backgroundColor: '#F9F8F6',
      textAlign: 'center',
      fontSize: '13px',
      fontFamily: 'Arial',
      borderRadius: '3px',

    },


}

    return (
      <div style={style_search.box}>
        <input style={style_search.color} type="text" placeholder="Search" />
      </div>
    );
  }
}
