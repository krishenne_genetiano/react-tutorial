import React, { Component } from 'react';


export default class Body extends Component {
  constructor(props){
    super(props);

    this.state = {
      tweet:''
    }
  }
    onInputChange(e){
      this.setState({
        tweet:e.target.value
      })
    }
  render() {
    return (
      <div>
      <input type ="text" name ="tweet" value ={this.state.tweet}
      onChange={this.onInputChange}/>
      </div>

    );
  }
}
