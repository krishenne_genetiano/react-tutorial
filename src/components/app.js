import React, { Component } from 'react';

import Header from './header/header_main';
import Post from './post1/post_main';
import Footer from './footer/footer_main';

// This is the main component
export default class App extends Component {
  render() {
    const bgcolormain = {
      backgroundColor: '#F9F8F6'
    }

    return (
      <div>
        <div style={bgcolormain}>
          <Header />
          <Post />
          <Footer />
        </div>
      </div>
    );
  }
}
